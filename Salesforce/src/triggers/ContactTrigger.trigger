trigger ContactTrigger on Contact (after insert, after update) {
    ContactTriggerHandler.contactHandler(Trigger.newMap, Trigger.operationType);
}
trigger AccountContacts on Account (after update) {

    set<id> ids = new set<id>();
    for(account a: trigger.new) {
        if(trigger.oldmap.get(a.id).Phone != a.Phone) {
            ids.add(a.id);
        }
    }
    
    system.debug('TRIGGER OLD MAP -- ' + trigger.oldmap);
   /* system.debug('TRIGGER NEW -- ' + trigger.oldmap);
    system.debug('TRIGGER OLD -- ' + trigger.oldmap);
    system.debug('IDS -- ' + trigger.oldmap);*/
    
    list<contact> finalacclist = new list<contact>();
    for(account acc: [select id,phone,(select id,phone from contacts) from account where id in : ids]) //trigger.new for all time edit 
    {
        
            for(contact cn : acc.contacts)
            {
                cn.Phone = acc.Phone;
                finalacclist.add(cn);
            }
            
    }
    
    //system.debug('finalacc -- ' + trigger.oldmap);
    
    //system.debug('finalacc'+' '+ finalacclist);
     update finalacclist;
    
    
    
}
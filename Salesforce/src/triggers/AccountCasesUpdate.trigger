trigger AccountCasesUpdate on Account (after update) {
    
    set<id> ids = new set<id>();
    for(account a: trigger.new) {
        if(trigger.oldmap.get(a.id).Phone != a.Phone) {
            ids.add(a.id);
        }
    }
    list<case> finalacclist = new list<case>();
    for(account acc: [select id,phone,(select id,phone__C from cases) from account where id in : ids]) //trigger.new for all time edit 
    {
        
            for(case cn : acc.cases)
            {
                cn.Phone__c = acc.Phone;
                finalacclist.add(cn);
            }
            
    }
    
    system.debug('finalacc'+' '+ finalacclist);
     update finalacclist;
}
trigger Duplicate_Records on Employee__c (before insert, before update) {
Set<string> empset  = new Set<String>();
    for(Employee__c e1: trigger.new) {
        empset.add(e1.name);
    }
    
    List<Employee__c> emplist = [select id,name from Employee__c where name in : empset];
    
    Map<string, Employee__c> empmap= new Map<string, Employee__c>();
    for(Employee__c e: emplist) {
        empmap.put(e.name, e);
    }
    
        
    system.debug(empset);
    system.debug(emplist);
    system.debug(empmap);
    system.debug(trigger.new);
    
    for(Employee__c e: trigger.new) {
        //if(empmap.get(e.name) != NULL) {
        if(empmap != NULL && empmap.containsKey(e.Name)){
            e.adderror('Duplicate Record Found');
        }
    }
    

    
    
  
}
public class Acc3_ctrl {
    public boolean FieldFlag{get;set;}
    public string AccId{get;set;}
    public List<Account> LAcc { get; set; }
    
    public Acc3_ctrl (){
        FieldFlag=False;    
        LAcc=[select Name,AnnualRevenue,NumberOfEmployees from Account];
    }
   
    public Account Acc;
    
    public void EditAction() {
        FieldFlag = True;
        AccId = ApexPages.currentPage().getParameters().get('AccIdParam');
    }

    public void CancelAction() {
        FieldFlag = false;
    }

    public void SaveAction() {
        FieldFlag = false;
        Update LAcc;
    }
    public void DeleteAction(){
        AccId = ApexPages.currentPage().getParameters().get('AccIdParam');
        Account A= [select Id from Account where id=: AccId];
        Delete A;
        LAcc=[select Name,AnnualRevenue,NumberOfEmployees from Account];
    }
        
}
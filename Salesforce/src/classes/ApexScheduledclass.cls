global class ApexScheduledclass implements schedulable {
    global void execute(schedulablecontext sc) {
        sendemail();
    }
    public void sendemail() {
        Messaging.Singleemailmessage email = new messaging.singleemailmessage();
        string[] toaddress = new string[]{'jalandhar.jntu@gmail.com'};
        email.setsubject('Testing Apex scheduled');
        email.setplaintextbody('test apex body');
        email.settoaddresses(toaddress);
        messaging.sendemail(new messaging.singleemailmessage[]{email}); 
    }
}
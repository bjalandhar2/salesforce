public with sharing class ContactTriggerHandler{
    public static void contactHandler(Map<Id, Contact> contactIdAndContactMap, System.TriggerOperation triggerEvent){
        Set<Id> accountIdsSet = new Set<Id>();
        switch on triggerEvent {
            when AFTER_INSERT, AFTER_UPDATE {
                for(Contact conRec: contactIdAndContactMap.values()){
                    accountIdsSet.add(conRec.AccountId);
                }
            }
        }
        updateAccountStatus(accountIdsSet);
    }
    
    public static void updateAccountStatus(Set<Id> accountIdsSet){
        List<Account> accountsToUpdate = new List<Account>();
        Map<Id, Integer> accountIdandActiveCountMap = new Map<Id, Integer>();
        for(Account acc: [SELECT Id, Is_Active__c,(SELECT Id FROM Contacts WHERE Is_Active__c = TRUE) FROM Account WHERE Id IN: accountIdsSet]){
            accountIdandActiveCountMap.put(acc.Id, acc.Contacts.size());
        }
        if(accountIdandActiveCountMap != NULL && !accountIdandActiveCountMap.isEmpty()){
            for(Id accountId: accountIdandActiveCountMap.keySet()){
                Account acc = new Account(Id = accountId);
                acc.Is_Active__c = FALSE;
                if(accountIdandActiveCountMap.containsKey(accountId) && accountIdandActiveCountMap.get(accountId) > 0){
                    acc.Is_Active__c = TRUE;
                }
                accountsToUpdate.add(acc);
            }
        }
        if(!accountsToUpdate.isEmpty()){
            update accountsToUpdate;
        }
    }
}